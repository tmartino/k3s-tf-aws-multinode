# EIP
resource "aws_eip" "eip" {
  vpc = true

  tags = {
    Name = "${var.project_name}_eip"
  }
}

# NATGW
resource "aws_nat_gateway" "natgw" {
  allocation_id = aws_eip.eip.id
  subnet_id     = aws_subnet.sbnt_public1.id

  tags = {
    Name = "${var.project_name}_natgw"
  }
}
