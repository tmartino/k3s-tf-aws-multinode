# EC2

# Master
resource "aws_instance" "master" {
  count           = 1
  ami             = var.aws_ec2_ami
  subnet_id       = aws_subnet.sbnt_public1.id
  instance_type   = var.aws_instance_type
  security_groups = [aws_security_group.sg_public.id]
  iam_instance_profile = aws_iam_instance_profile.ec2_profile.name
  user_data       = file("postinstall-m.sh")

  # OS disk
  root_block_device {
    volume_size           = "20"
    volume_type           = "gp2"
    encrypted             = false
    delete_on_termination = true
  }

  tags = {
    Name = "master-${count.index + 1}"
  }

  provisioner "local-exec" {
    command = "echo ${self.public_ip} >> public_ips.txt"
  }
}

# Workers
resource "aws_instance" "worker1" {
  ami             = var.aws_ec2_ami
  subnet_id       = aws_subnet.sbnt_public1.id
  instance_type   = var.aws_instance_type
  security_groups = [aws_security_group.sg_public.id]
  iam_instance_profile = aws_iam_instance_profile.ec2_profile.name
  user_data       = file("postinstall-w.sh")

  tags = {
    Name = "worker1"
  }

  provisioner "local-exec" {
    command = "echo ${self.public_ip} >> public_ips.txt"
  }
}

resource "aws_instance" "worker2" {
  ami             = var.aws_ec2_ami
  subnet_id       = aws_subnet.sbnt_public1.id
  instance_type   = var.aws_instance_type
  security_groups = [aws_security_group.sg_public.id]
  iam_instance_profile = aws_iam_instance_profile.ec2_profile.name
  user_data       = file("postinstall-w.sh")

  tags = {
    Name = "worker2"
  }

  provisioner "local-exec" {
    command = "echo ${self.public_ip} >> public_ips.txt"
  }
}

resource "aws_instance" "worker3" {
  ami             = var.aws_ec2_ami
  subnet_id       = aws_subnet.sbnt_public1.id
  instance_type   = var.aws_instance_type
  security_groups = [aws_security_group.sg_public.id]
  iam_instance_profile = aws_iam_instance_profile.ec2_profile.name
  user_data       = file("postinstall-w.sh")

  tags = {
    Name = "worker3"
  }



  provisioner "local-exec" {
    command = "echo ${self.public_ip} >> public_ips.txt"
  }
}
