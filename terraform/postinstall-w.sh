#!/bin/bash
# This scripts performs postinstall configuraton on deployed VMs.

# Variables
LBIP=`hostname -I | awk '{print $1}'`

# Install packages
yum -y install git

# hostname
#hostnamectl set-hostname worker --static

# Create User
useradd -s /bin/bash -c "Admin" -m admin
echo "Passw0rd" | passwd --stdin admin
# Set sudo
echo "admin ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers
# Deploy SSH keys
mkdir /home/admin/.ssh
cat <<EOF | tee -a /home/admin/.ssh/authorized_keys
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIB+FcJU11IIvmuF+YGQTCjjshbAVHWGrbsrovA6bp9C5 Peter Barczi, pety.barczi@gmail.com
EOF
# Set proper permissions
chown -R admin /home/admin/.ssh
chmod 700 /home/admin/.ssh
chmod 600 /home/admin/.ssh/authorized_keys
##

## Init K3S
curl -sfL https://get.k3s.io | sh -
systemctl stop k3s
#

# Join to the Cluster
sleep 4m
wget http://master.pety.net:8080/token 
token=`cat token`
master_ip=`host master.pety.net | cut -d " " -f4`
k3s agent --server https://${master_ip}:6443 --token ${token}

## Add aliases
cat <<EOF | tee -a /etc/bashrc
# Aliases
alias s='sudo su -'
alias k='k3s kubectl'
alias c=clear
EOF
##

## Motd
unlink /etc/motd
cat <<EOF | tee -a /etc/motd
┌─────────────────────────────────────────────────────────────┐
│┌───────────────────────────────────────────────────────────┐│
││                 Welcome to OSC Environment!               ││
││───────────────────────────────────────────────────────────││
││                                                           ││
││             This is your K3S multi-node cluster.          ││
││                                                           ││
│└───────────────────────────────────────────────────────────┘│
└─────────────────────────────────────────────────────────────┘
EOF
##
